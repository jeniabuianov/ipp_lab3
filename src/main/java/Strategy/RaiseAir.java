package Strategy;

public class RaiseAir implements Strategy {
    AirconditionReceiver air;
    RaiseAir(AirconditionReceiver receiver){
        this.air = receiver;
    }

    @Override
    public void porcessAirTemp() {
        System.out.println("Your temperature is "+air.temperature+". It's too few. I'll raise it!");
    }
}
