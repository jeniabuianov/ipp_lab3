package Strategy;

public class LowAir implements Strategy {
    AirconditionReceiver air;

    LowAir(AirconditionReceiver receiver){
        this.air = receiver;
    }

    @Override
    public void porcessAirTemp() {
        System.out.println("Your temperature is "+air.temperature+". It's too much. I'll low it!");
    }
}
