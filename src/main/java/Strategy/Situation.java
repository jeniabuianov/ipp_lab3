package Strategy;

public class Situation {
    private Strategy strategy;

    public Situation(Strategy strategy){
        this.strategy = strategy;
    }

    public void handleByAir(){
        this.strategy.porcessAirTemp();
    }
}