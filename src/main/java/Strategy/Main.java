package Strategy;

public class Main {
    public static void main(String args[]){
        AirconditionReceiver air = new AirconditionReceiver(30);
        LowAir low = new LowAir(air);
        RaiseAir raise = new RaiseAir(air);

        Situation s1 = new Situation(low);
        Situation s2 = new Situation(raise);

        s1.handleByAir();
        s2.handleByAir();
    }
}
