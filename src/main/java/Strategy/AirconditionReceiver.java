package Strategy;

import State.AirOff;
import State.AirOn;
import State.AirPause;
import State.AirPlus;

public class AirconditionReceiver {
    int temperature = 0;

    AirconditionReceiver(int myTemperature){
        this.temperature = myTemperature;
    }

    public void on(){
        System.out.println("Switched on!");
    }

    public void off(){
        System.out.println("Switched off!");
    }

    public void setTemperature(int temperature){
        this.temperature = temperature;
    }
}
