package Command;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        HomeElectronics ce = UniversalRemote.getActiveDevice();
        LightSystemReceiver ls = new LightSystemReceiver();
        AirconditionReceiver ac = new AirconditionReceiver();
        List<HomeElectronics> all = new ArrayList<HomeElectronics>();
        all.add(ls);
        all.add(ac);

        OnCommand onCommandLight = new OnCommand(ls);
        OnCommand onCommandAir = new OnCommand(ac);

        onCommandAir.execute();
        OnCommand onCommandButton = new OnCommand(ce);
        ButtonInvoker onButton = new ButtonInvoker(onCommandButton);
        onButton.click();

        OffCommand offAll = new OffCommand(all);
        ButtonInvoker offAllButton = new ButtonInvoker(offAll);
        offAllButton.click();

    }
}
