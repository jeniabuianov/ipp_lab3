package Command;

public interface HomeElectronics {

    public abstract void on();
    public abstract void off();

}