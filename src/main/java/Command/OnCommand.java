package Command;

public class OnCommand implements Command {

    private HomeElectronics homeElectronics;

    public OnCommand (HomeElectronics homeElc){
        this.homeElectronics = homeElc;
    }

    @Override
    public void execute() {
        homeElectronics.on();
    }

}