package Command;

import java.util.List;

public class OffCommand implements Command {

    List<HomeElectronics> homeElecList;

    public OffCommand (List<HomeElectronics> homeElcList){
        this.homeElecList = homeElcList;
    }

    @Override
    public void execute() {
        for (HomeElectronics homeElec : homeElecList) {
            homeElec.off();
        }
    }
}