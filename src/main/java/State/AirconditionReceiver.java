package State;

public class AirconditionReceiver implements HomeElectronicState {

    HomeElectronicState stateOn;
    HomeElectronicState stateOff;
    HomeElectronicState statePause;
    HomeElectronicState statePlus;

    HomeElectronicState state;
    AirconditionReceiver(){
        this.stateOn = new AirOn(this);
        this.stateOff = new AirOff(this);
        this.statePause = new AirPause(this);
        this.statePlus = new AirPlus(this);

        this.state = this.stateOn;
    }

    public void setState(HomeElectronicState state){
        this.state = state;
    }

    public HomeElectronicState getStateOn() {
        return stateOn;
    }

    public HomeElectronicState getStateOff() {
        return stateOff;
    }

    public HomeElectronicState getStatePause() {
        return statePause;
    }

    public HomeElectronicState getStatePlus() {
        return statePlus;
    }

    @Override
    public void on() {
        state.on();
    }

    @Override
    public void off() {
        state.off();
    }

    @Override
    public void pause() {
        state.pause();
    }

    @Override
    public void plus_temp() {
        state.plus_temp();
    }
}
