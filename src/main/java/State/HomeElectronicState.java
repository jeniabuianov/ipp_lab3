package State;

public interface HomeElectronicState {
    public void on();
    public void off();
    public void pause();
    public void plus_temp();
}
