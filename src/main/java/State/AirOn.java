package State;

public class AirOn implements HomeElectronicState {
    private final AirconditionReceiver receiver;

    AirOn(AirconditionReceiver receiver){
        this.receiver = receiver;
    }


    @Override
    public void on() {
        System.out.println("Power On!");
    }

    @Override
    public void off() {
        System.out.println("Power Off!");
        receiver.setState(receiver.getStateOff());
    }

    @Override
    public void pause() {
        System.out.println("Power Pause!");
        receiver.setState(receiver.getStatePause());
    }

    @Override
    public void plus_temp() {
        System.out.println("Plus temp!");
        receiver.setState(receiver.getStatePlus());
    }
}
