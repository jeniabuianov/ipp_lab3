package State;

public class AirPause implements HomeElectronicState {
    private final AirconditionReceiver receiver;

    AirPause(AirconditionReceiver receiver){
        this.receiver = receiver;
    }


    @Override
    public void on() {

        System.out.println("Power On!");
        receiver.setState(receiver.getStateOn());
    }

    @Override
    public void off() {
        System.out.println("Power Off!");
        receiver.setState(receiver.getStateOff());
    }

    @Override
    public void pause() {
        System.out.println("Power Pause!");
    }

    @Override
    public void plus_temp() {
        System.out.println("Sorry. Cannot set pause. Conditions is paused!");
    }
}
