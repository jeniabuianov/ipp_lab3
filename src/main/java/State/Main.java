package State;

public class Main {

    public static void main(String[] args) {
        AirconditionReceiver receiver = new AirconditionReceiver();
        receiver.on();
        receiver.plus_temp();
        receiver.pause();
        receiver.off();

        receiver.plus_temp();
        receiver.on();
        receiver.plus_temp();
        receiver.off();
    }
}
