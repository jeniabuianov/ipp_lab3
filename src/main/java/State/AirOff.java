package State;

public class AirOff implements HomeElectronicState {
    private final AirconditionReceiver receiver;

    AirOff(AirconditionReceiver receiver){
        this.receiver = receiver;
    }


    @Override
    public void on() {
        System.out.println("Power On!");
        receiver.setState(receiver.getStateOn());
    }

    @Override
    public void off() {
        System.out.println("Power Off!");
    }

    @Override
    public void pause() {
        System.out.println("Sorry. Cannot set pause. Conditions is off");
    }

    @Override
    public void plus_temp() {
        System.out.println("Sorry. Cannot set pause. Conditions is off");
    }
}
