package Observer;


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        ShopList shops = new ShopList();
        shops.add(new Atrium());
        shops.add(new Elat());
        shops.add(new Jumbo());

        System.out.println("Please select action:");
        System.out.println("1.\tAdd brand");
        System.out.println("2.\tChange floor number");

        Scanner scanner = new Scanner(System.in);
        int action = scanner.nextInt();

        System.out.println("Please select magazine:");

        for(int i=0;i<shops.shops.size();i++){
            System.out.println((i+1)+".\t"+shops.getMagazine(i).getName());
        }

        int magazine = scanner.nextInt()-1;

        if(action==1){
            System.out.println("Please enter brand: ");
            scanner = new Scanner(System.in);
            String brand = scanner.nextLine();
            shops.getMagazine(magazine).setBrand(brand);
        }
        else{
            System.out.println("Please enter new floor number more then "+shops.getMagazine(magazine).getNumberFloors());
            int floors = scanner.nextInt();
            shops.getMagazine(magazine).setNumberFloors(floors);
        }
    }
}
