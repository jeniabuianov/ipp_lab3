package Observer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Elat extends Magazine {
    Elat(){
        this.setName("Elat");
        this.setNumberFloors(3);
        this.setNumberOfButOnLevel(2);
        this.setDefaultBrands();
        this.defineEvents();
    }

    private void setDefaultBrands(){
        HashMap<Integer,List<String>> brands = new HashMap<>();
        List<String> firstFloor = new ArrayList<>();
        firstFloor.add("Zorile");
        firstFloor.add("Suschimania");

        List<String> secondFloor = new ArrayList<>();
        secondFloor.add("MaxFashion");

        brands.put(1,firstFloor);
        brands.put(2,secondFloor);
        this.setBrands(brands);
    }
}
