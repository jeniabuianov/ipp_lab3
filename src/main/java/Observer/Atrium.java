package Observer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Atrium extends Magazine {

    Atrium(){
        this.setName("Atrium");
        this.setNumberFloors(5);
        this.setNumberOfButOnLevel(3);
        this.setDefaultBrands();
        this.defineEvents();
    }

    private void setDefaultBrands(){
        HashMap<Integer,List<String>> brands = new HashMap<>();
        List<String> firstFloor = new ArrayList<>();
        firstFloor.add("CoffeeBeans");
        firstFloor.add("Vizaj Nica");
        firstFloor.add("Librarius");

        List<String> secondFloor = new ArrayList<>();
        secondFloor.add("Icos");
        secondFloor.add("Esigarete");
        secondFloor.add("Luminare");

        List<String> thierdFloor = new ArrayList<>();
        thierdFloor.add("Bulgarian Rose");

        brands.put(1,firstFloor);
        brands.put(2,secondFloor);
        brands.put(3,thierdFloor);
        this.setBrands(brands);
    }
}
