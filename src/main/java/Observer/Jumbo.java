package Observer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Jumbo extends Magazine {
    Jumbo(){
        this.setName("Jumbo");
        this.setNumberFloors(3);
        this.setNumberOfButOnLevel(5);
        this.setDefaultBrands();
        this.defineEvents();
    }

    private void setDefaultBrands(){
        HashMap<Integer,List<String>> brands = new HashMap<>();
        List<String> firstFloor = new ArrayList<>();
        firstFloor.add("Maib");
        firstFloor.add("Fidesco");
        firstFloor.add("Librarius");
        firstFloor.add("Orange");
        firstFloor.add("Moldcell");


        List<String> secondFloor = new ArrayList<>();
        secondFloor.add("Sex Shop");

        brands.put(1,firstFloor);
        brands.put(2,secondFloor);
        this.setBrands(brands);
    }
}
