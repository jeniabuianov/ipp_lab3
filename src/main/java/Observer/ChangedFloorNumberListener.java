package Observer;

public class ChangedFloorNumberListener implements IEventListener {

    @Override
    public void update(String eventType, String magazine) {
        System.out.println("Someone "+eventType+" to "+magazine);
    }
}
