package Observer;

import ChainOfResponsability.Middleware;

import java.util.ArrayList;
import java.util.List;

public class ShopList {
    List<Shop> shops = new ArrayList<>();

    public void add(Shop magazine){
        shops.add(magazine);
    }

    public void showAllBrands(){
        for (int i=0;i<shops.size();i++){
            shops.get(i).showBrands();
        }
    }

    public Shop getMagazine(int id){
        return shops.get(id);
    }
}
