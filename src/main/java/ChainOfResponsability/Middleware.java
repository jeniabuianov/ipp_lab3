package ChainOfResponsability;

public abstract class Middleware {
    private Middleware next;

    public Middleware linkWith(Middleware next) {
        this.next = next;
        return next;
    }

    public abstract boolean check(String brand);

    protected boolean checkNext(String brand) {
        if (next == null) {
            return true;
        }
        return next.check(brand);
    }
}