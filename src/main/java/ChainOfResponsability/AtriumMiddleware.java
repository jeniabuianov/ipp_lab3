package ChainOfResponsability;

public class AtriumMiddleware extends Middleware {
    private Magazine magazine;

    AtriumMiddleware(Magazine magazine){
        this.magazine = magazine;
    }

    @Override
    public boolean check(String brand) {
        if (magazine.findBrandByName(brand)>0){
            System.out.println("Found in "+magazine.getName()+" on "+magazine.findBrandByName(brand)+" floor");
            return true;
        }
        return checkNext(brand);
    }
}
