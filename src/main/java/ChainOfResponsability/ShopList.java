package ChainOfResponsability;

import java.util.ArrayList;
import java.util.List;

public class ShopList {
    List<Shop> shops = new ArrayList<>();
    private Middleware middleware;

    public void setMiddleware(Middleware middleware) {
        this.middleware = middleware;
    }

    public void add(Shop magazine){
        shops.add(magazine);
    }


    public boolean findBrand(String brand) {
        if (middleware.check(brand)) {
            return true;
        }
        return false;
    }

    public void showAllBrands(){
        for (int i=0;i<shops.size();i++){
            shops.get(i).showBrands();
        }
    }

    public Shop getMagazine(int id){
        return shops.get(id);
    }
}
