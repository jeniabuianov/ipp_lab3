package ChainOfResponsability;

public class JumboMiddleware extends Middleware {
    private Magazine magazine;

    JumboMiddleware(Magazine magazine){
        this.magazine = magazine;
    }

    @Override
    public boolean check(String brand) {
        if (magazine.findBrandByName(brand)>0){
            System.out.println("Found in "+magazine.getName()+" on "+magazine.findBrandByName(brand)+" floor");
            return true;
        }
        return checkNext(brand);
    }
}
