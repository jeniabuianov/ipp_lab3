package ChainOfResponsability;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args){

        ShopList shops = new ShopList();
        shops.add(new Atrium());
        shops.add(new Elat());
        shops.add(new Jumbo());

        Middleware middleware = new AtriumMiddleware((Magazine) shops.getMagazine(0));
        middleware.linkWith(new ElatMiddleware((Magazine) shops.getMagazine(1)))
                    .linkWith(new JumboMiddleware((Magazine) shops.getMagazine(2)));

        shops.setMiddleware(middleware);


        System.out.println("Please select brand from list");
        shops.showAllBrands();

        System.out.println("\n\n\tWrite brand name");
        Scanner scanner = new Scanner(System.in);
        String selected = scanner.nextLine();

        if (!shops.findBrand(selected)){
            System.out.println("We cannot find this brand");
        }

    }
}
